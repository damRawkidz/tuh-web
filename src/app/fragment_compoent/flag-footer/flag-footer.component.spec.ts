import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlagFooterComponent } from './flag-footer.component';

describe('FlagFooterComponent', () => {
  let component: FlagFooterComponent;
  let fixture: ComponentFixture<FlagFooterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlagFooterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlagFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
