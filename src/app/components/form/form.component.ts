import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { Planing } from 'src/app/models/planing';
import { AppService } from 'src/app/app.service';
import { distinctUntilChanged, debounceTime } from 'rxjs/operators';
@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  form: FormGroup
  searchForm :FormGroup
  plansArray :Planing[]
  testyear = '2009'
  saveArray = []
  constructor(private fb: FormBuilder,private service:AppService) {
    this.form = this.initForm()
    this.searchForm  = this.initsearchForm()
   }

   initsearchForm(){
     this.testyear = new Date().getFullYear().toString()
      return this.fb.group({
        year:[+new Date().getFullYear() + 543]
    })
   }
  ngOnInit() {
    this.searchForm.get('year').valueChanges.pipe(
      distinctUntilChanged(),
      debounceTime(1000)
    ).subscribe( (year) => {      
      let convertyear = +year
      this.testyear = (convertyear - 543).toString()
      this.loadData()
    })
    this.loadData()
  }

  loadData(){
    let form = this.form.get('data') as FormArray
    form.controls.length = 0
    const yearId:[{}] = [{
      yearId: this.testyear
    }]
    this.service.callService<Planing>('get',yearId)
        .toPromise().then( res => {
          console.log(res)
          if(res.data.length != 0){
            res.data.map((plan,index,plans:Planing[]) => {
              this.plansArray = plans
              if(plan.levelId == 0){
                form.push(this.formServer(plan))
              }
            })
            this.addformlevel1(form)
            this.addformLevel2(form)
            this.addformLevel3(form) 
          }
        })
  }
  addformlevel1(form:FormArray){            
      form.controls.map( form => {
        let parentlevel1 = this.plansArray.filter(plan => plan.levelId == 1 && plan.parentId == form.get('id').value)
        parentlevel1.map(x => {
          (form.get('parent') as FormArray).push(this.formServer(x));
        })
      })      
  }

  addformLevel2(form:FormArray){
    form.controls.map( form => {
      (form.get('parent') as FormArray).controls.map( form => {
        let parentlevel2 = this.plansArray.filter(plans => plans.levelId == 2 && plans.parentId == form.get('id').value)
        parentlevel2.map( x => {
          (form.get('parent') as FormArray).push(this.formServer(x));
        })        
      })
    })
  }

  addformLevel3(form:FormArray){
    form.controls.map( form => {
      (form.get('parent') as FormArray).controls.map( form => {
       (form.get('parent') as FormArray).controls.map( form => {
        let parentlevel3 = this.plansArray.filter(plans => plans.levelId == 3 && plans.parentId == form.get('id').value)
        parentlevel3.map( plan => {
          (form.get('parent') as FormArray).push(this.formServer(plan))
        })
       })
      })
    })
  }


  addform(level:number,mForm?:FormGroup){    
    if(mForm != undefined){
      (<FormArray>mForm.get('parent')).push(this.newForm(level));
    } else {
      let form = this.form.get('data') as FormArray
      form.push(this.newForm(level))
    }
  }

  remove(index,mform?:FormGroup){
    if(mform != undefined){
      (<FormArray>mform.get('parent')).removeAt(index)
    } else {
      let form = this.form.get('data') as FormArray
      form.removeAt(index)
      
    }
  }
  newForm(levelId:number = 0){
    return this.fb.group({
      yearId: this.testyear,
      levelId: levelId,
      budgetCode: '',
      budgetName: '',
      planAmount: 0,
      planAdjust1: 0,
      planAdjust2: 0,
      planTotal: 0,
      actualQ1: 0,
      actualQ2: 0,
      actualQ3: 0,
      actualQ4: 0,
      actualLeft: 0,
      actualTotal: 0,
      parentId: 0,
      id: 0,
      parent:this.fb.array([])
    })
  }

  formServer(plan:Planing){
    return this.fb.group({
      yearId: plan.yearId,
      levelId: plan.levelId,
      budgetCode: plan.budgetName,
      budgetName: plan.budgetName,
      planAmount: plan.planAmount,
      planAdjust1: plan.planAdjust1,
      planAdjust2: plan.planAdjust2,
      planTotal: plan.planTotal,
      actualQ1: {value:plan.actualQ1,disabled:true},
      actualQ2: {value:plan.actualQ2,disabled:true},
      actualQ3: {value:plan.actualQ3,disabled:true},
      actualQ4: {value:plan.actualQ4,disabled:true},
      actualLeft: plan.actualLeft,
      actualTotal: plan.actualTotal,
      parentId: plan.parentId,
      id: plan.id,
      parent:this.fb.array([])
    })
  }

  save(){
    this.saveArray = []
    let value = this.form.get('data').value as Planing[]    
    value.map(x => {
      this.saveArray.push(x)
      x.parent.map( a => {
        this.saveArray.push(a)
        a.parent.map( s => {
          this.saveArray.push(s)
          s.parent.map( d => {
            this.saveArray.push(d)
          })
        })
      })
    })
    console.log(this.saveArray)
    this.service.callService('new',this.saveArray).toPromise()
      .then(res => {
        alert(res.apiResponse.desc)
        console.log(res);
      })
  }

  initForm(){
      return this.fb.group({
        data:this.fb.array([
          // this.newForm()
        ])
    })
  }

}
