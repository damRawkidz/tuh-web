import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms'
import { Routes, RouterModule } from "@angular/router";
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { FlagComponent } from './fragment_compoent/flag/flag.component';
import { FormComponent } from './components/form/form.component';
import { FlagFooterComponent } from './fragment_compoent/flag-footer/flag-footer.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

export const routes:Routes = [
  {
    path:'',
    component:DashboardComponent
  },
  {
    path:'dashboard',
    component:DashboardComponent      
  }
]
@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    FlagComponent,
    FormComponent,
    FlagFooterComponent,  
  ],
  imports: [
    BrowserModule,
    HttpClientModule,    
    ReactiveFormsModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(routes),
  ],
  providers: [],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
