import { Component, OnInit, Input, HostBinding } from '@angular/core';


@Component({
  selector: 'flag-footer',
  templateUrl: './flag-footer.component.html',
  styleUrls: ['./flag-footer.component.css'],
})
export class FlagFooterComponent implements OnInit {
  @Input() type:string
  @Input() detail:string
  @Input() moreMargin:boolean = true
  @Input() color :string
  @HostBinding('style.margin') private margin = this.moreMargin ? '10px' :'10px 20px 20px 20px'
  @HostBinding('style.width') private width = '100%'
  constructor() { }
  
  ngOnInit() {
  }

}
