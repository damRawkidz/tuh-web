import { Component, OnInit, OnDestroy } from '@angular/core';
import { interval, Observable, Subscription } from 'rxjs';
import { AppService } from 'src/app/app.service';
import { Hospital, queueDetail, summary } from 'src/app/models/realtime';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit,OnDestroy {
  intervalSubscription = new Subscription();

  medicineOne:queueDetail 
  surgery:queueDetail
  women:queueDetail
  baby:queueDetail
  ear_nose_and_throat:queueDetail
  eye:queueDetail
  skeleton:queueDetail
  generalAndFamily:queueDetail
  medicineTwo:queueDetail 
  surgeryTwo:queueDetail
  pillRoom:queueDetail
  summary:summary
  isSHowFirstGroup:boolean = true
  queues = []
  constructor(private service:AppService) { }

  ngOnInit() {    
    this.fetchApi()
  }

  fetchApi(){
    this.intervalSubscription = interval(10000).subscribe(count => {
      this.isSHowFirstGroup = true
      this.service.realTimeAPi<Hospital>().toPromise().then(res => {
        this.queues = res.queues.slice(0,8)
        console.log(res.queues)
        this.medicineOne = res.queues[0]
        this.surgery = res.queues[1]
        this.women = res.queues[2]
        this.baby = res.queues[3]
        this.ear_nose_and_throat = res.queues[4]
        this.eye = res.queues[5]
        this.skeleton = res.queues[6]
        this.generalAndFamily = res.queues[7]
        this.medicineTwo = res.queues[8]
        this.surgeryTwo = res.queues[9]
        // this.pillRoom = res[10]
        if(res.queues.length > 8){
          setTimeout(() => {
            this.queues = res.queues.slice(8,res.queues.length)
          }, 5000);
        }
        this.summary = res.summary
      })
    })
  }

  ngOnDestroy(): void {
    this.intervalSubscription.unsubscribe()
  }
}
