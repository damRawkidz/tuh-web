import { Component, OnInit, Input, HostBinding, ChangeDetectionStrategy, OnChanges, SimpleChanges } from '@angular/core';
import { Flip } from 'src/app/utility/animation';


@Component({
  selector: 'app-flag',
  templateUrl: './flag.component.html',
  styleUrls: ['./flag.component.css'],
  changeDetection:ChangeDetectionStrategy.OnPush,
  animations:[Flip()]
})
export class FlagComponent implements OnInit,OnChanges { 
  @Input() nameClinic: string
  @Input() nameClinicEng: string
  @Input() totleQueue: number = 0
  @Input() statusQueue: string 
  @Input() image: string
  @Input() timeToWait: string
  @Input() color: string
  @Input() moreMargin:boolean = true
  @HostBinding('style.margin') private margin = this.moreMargin ? '10px' :'10px 20px 20px 20px'
  @HostBinding('style.width') private width = '100%'
  flipState:boolean = true
  constructor() { }

  ngOnChanges(changes: SimpleChanges): void {
      this.flipState = !this.flipState
      console.log(this.flipState)
  }
  ngOnInit() {
    
  }

  // flipingState(){
  //   this.flipState == 'front' ? this.flipState = 'back' : this.flipState = 'front'
  //   console.log(this.flipState)
  // }
  dynamicClass(){
    if(+this.timeToWait < 60){
      return 'green'
    } else if (+this.timeToWait >= 60 && +this.timeToWait <= 120 ){
      return 'yellow'
    } else if(+this.timeToWait > 120) {
      return 'red'
    }
  }
}
