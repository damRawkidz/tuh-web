// export interface Hospital {    
//         code:string,
//         name: string,
//         queue: number,
//         waittime:number,
// }

export interface Hospital {           
        queues: queueDetail[],
        summary:summary
        
}
export interface summary {
        total:number,
        finish:number
}
export interface queueDetail {
        code: string, 
        name: string, 
        nameEn: string, 
        queue: number, 
        waittime: number
}