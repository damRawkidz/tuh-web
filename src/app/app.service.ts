import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApiResponse } from './models/http/api-response';


@Injectable({
  providedIn: 'root'
})

export class AppService {
  private prefix:string = 'http://tuhweb.thamming.com/api/'
  private endpoint:string = 'planning'
  public url:string = ''
  constructor(private http:HttpClient) {
    this.url = this.prefix + this.endpoint
   }

  callService<T>(actions:string,data?):Observable<ApiResponse<T>>{
    const request = {
      apiRequest:{action:actions},
      data:data || []
    }
    return this.http.post<ApiResponse<T>>(this.url,request)
  }
  
  realTimeAPi<T>(){
    const request = {
      apiRequest:{action:'get'}
    }
    // return this.http.post<T>('http://192.168.149.31/api/opd_dashboard',request);
    // return this.http.post<T>('http://tuhweb.thamming.com/api/realtime',request);
    return this.http.post<T>('http://tuhweb.thamming.com/api/opddashboard',request);
  }
}
