export interface Planing {
    actualLeft: number
    actualQ1: number
    actualQ2: number
    actualQ3: number
    actualQ4: number
    actualTotal: number
    budgetCode: string
    budgetName: string
    id: number
    levelId: number
    parentId: number
    planAdjust1: number
    planAdjust2: number
    planAmount: number
    planTotal: number
    yearId: number
    parent:Planing[]
}